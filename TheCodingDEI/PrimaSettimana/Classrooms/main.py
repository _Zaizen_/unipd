import sys
from sortedcontainers import SortedList, SortedSet
from more_itertools import peekable

values = []

counter = 0
k = 0
n = 0
for i in sys.stdin:
    ab = i.split()
    a = int(ab[0])
    b = int(ab[1])

    if (counter == 0):
        k = a
        n = b
    else:
        values.append((a,b))
    
    counter += 1

values = sorted(sorted(values, key = lambda x : x[0]), key = lambda x : x[1])  
#print(values)

risposta = 0

res_values  = []

for i in range(k):
    #print(str(chr(i+65)))

    valore = ""
    
    # Whether there is a classroom activity that 
    # ends before the start time of i
    #print(-values[i][0])
    for dato in res_values:
        #print(dato)
        if dato[0] >= -values[i][0]:
            valore = dato[0]
            break
    #print(valore)
    #if len(res_values) != 0:
        #print(str(res_values) + " primo")
    
    if valore == "":
        #print("here")
        if len(res_values) < n:
            found = False
            for dato in reversed(res_values):
                if dato[0] == -values[i][1]-1:
                    dato.count += 1
                    found = True
            if not found:
                res_values.append((-values[i][1]-1, 1))
                #res_values[str(-values[i][1]-1)] = 1
                res_values = sorted(res_values)
            #print(str(res_values) + " secondo")
            risposta += 1
        continue

    for dato in res_values:
        if dato[0] == valore:
            dato_buffer = (valore, dato[1]-1)
            if dato_buffer[1] == 0:
                res_values.remove((valore,1))

    res_values.append((-values[i][1]-1,1))
    res_values = sorted(res_values)
    risposta+=1
    #print(str(res_values) + " terzo")
    
#print(risposta)
    
'''
res_values = []

for i in range(k):
    #print(str(chr(i+65)))

    valore = ""
    
    # Whether there is a classroom activity that 
    # ends before the start time of i
    for dato in res_values:
        if dato > -values[i][0]:
            valore = dato
            break
    
    if len(res_values) != 0:
        #print(str(res_values) + " primo")
    
    if valore == "":
        #print("here")
        if len(res_values) < n:
            res_values.append(-values[i][1]-1)
            res_values.sort(key=lambda x: x)
            #print(str(res_values) + " secondo")
            risposta += 1
        continue
    res_values.remove(valore)
    res_values.append(-values[i][1]-1)
    res_values.sort(key=lambda x: x)
    risposta+=1
    #print(str(res_values) + " terzo")
    
#print(risposta)


'''
risposta = 0

tempi_di_fine = []
already_added_index = []

for i in range(0, k):
    #print("a")
    attivita_finita = ""
    # Whether there is a classroom activity that ends before the 
    # start time of i
    index = 0
    for tempo_di_fine in tempi_di_fine:
        if (tempo_di_fine < -values[i][0]):
            attivita_finita = tempo_di_fine
            break
        index += 1
    
    #print(str(tempi_di_fine)+ " primo")

    if (attivita_finita == ""):
        #print("here")
        if (len(tempi_di_fine) < n):
            tempi_di_fine.append(-values[i][1]-1)
            tempi_di_fine.sort(key=lambda x: x)
            #print(str(tempi_di_fine)+" secondo")
            risposta += 1
        continue

    tempi_di_fine.pop(index)
    #tempi_di_fine.remove(attivita_finita)
    tempi_di_fine.append(-values[i][1]-1)
    tempi_di_fine.sort(key=lambda x: x)
    risposta += 1
    #print(str(tempi_di_fine) + " terzo")


#print(risposta)


#print(values)
#print(k,n)

risposta = 0

tempi_di_fine = []
already_added_index = []

for i in range(0,k): # k = 4
    #print("##########")
    #print(values[i])
    if (len(tempi_di_fine) < n): # n= 2
        #print("here")
        tempi_di_fine.append(values[i][1])
        tempi_di_fine.sort(key=lambda x: x)
        already_added_index.append(i)
        risposta+=1
    else:
        for tempo in reversed(tempi_di_fine):
            #print(tempi_di_fine)
            if (tempo < values[i][0]):
                tempi_di_fine.remove(tempo)
                tempi_di_fine.append(values[i][1])
                tempi_di_fine.sort(key=lambda x: x)
                risposta+=1
                #print(tempi_di_fine)
                break
    #print(tempi_di_fine)

    
#print(tempi_di_fine)

#print(risposta)

endtime= SortedSet()

risposta = 0

for i in range(k):
    #iterator  = endtime.bisect_left(-values[i][0])
    start = endtime.bisect_left(-values[i][0])
    iterator = iter(endtime[x] for x in range(start, len(endtime)))
    #iterator = iter(endtime[x] for x in range(start, len(endtime)))
    iterator = peekable(iterator)


    if not iterator:
        #print("here")
        if len(endtime) < n:
            endtime.add(-values[i][1] -1)
            risposta += 1
            #print(endtime)
        continue

    #print(endtime)

    endtime.remove(iterator.peek())
    #del endtime[iterator]
    endtime.add(- values[i][1] -1)
    risposta += 1

print(risposta)




import sys
import bisect

values = []

counter = False

for i in sys.stdin:
    ab = i.split()
    a = int(ab[0])
    b = int(ab[1])

    if counter:
        values.append((a, b))
    else:
        k = a
        n = b
        counter = True

classes = []
risposta = 0

for i in range(k):
    valid = True
    j = 0
    if len(classes) == 0:
        classes.append([values[i]])
        risposta+=1
    elif len(classes) < n:
        for classroom in classes:
            if not valid:
                break
            for attivita in classroom:
                if attivita[1] < values[i][0]:
                    pass
                else:
                    classes.append([values[i]])
                    risposta+=1
                    valid = False
                    break
            if valid:
                classroom.append(values[i])
                risposta+=1
                break
            j+=1
    else:
        for classroom in classes:
            valid=True
            for attivita in classroom:
                if attivita[1] < values[i][0]:
                    pass
                else:
                    valid = False
                    break
            if valid:
                classroom.append(values[i])
                risposta+=1
                break
            j += 1
        
print(risposta)


