# Se vuoi testare lo script usa il comando:
# cat dataset1.txt | python3 final.py && cat dataset2.txt | python3 final.py

import sys
import bisect

values = []

counter = 0
k = 0
n = 0
for i in sys.stdin:
    ab = i.split()
    a = int(ab[0])
    b = int(ab[1])

    if (counter == 0):
        k = a
        n = b
    else:
        values.append((a, b))

    counter += 1

values.sort(key=lambda x: (x[1], x[0]))

endtime = []

risposta = 0

for i in range(k):

    base_value = values[i]

    value = -base_value[0]

    start = bisect.bisect_left(endtime, value)

    value = -base_value[1]-1
    
    if start >= len(endtime):
        if len(endtime) < n:
            bisect.insort(endtime, value)
            risposta += 1
        continue
    
    del endtime[start]
    bisect.insort(endtime, value)
    risposta+=1

print(risposta)
