
#include <algorithm>
#include <cstring>
#include <string.h>
#include <iostream>
#include <list>
#include <map>
#include <set>
#include <stack>
#include <string>
#include <utility>
#include <vector>
#include <cstdio>
#include <cmath>

#define LL long long
#define N 200005
#define INF 0x3ffffff

using namespace std;

int n , m;
struct node{
    int bg, ed; //m start time and end time of each event
}a[N];

bool cmp(node a , node b){ // sorted by end time
    if(a.ed== b.ed) 
        return a.bg < b.bg;
    return a.ed< b.ed;
}

int main(){
    while (~scanf("%d%d" , &n , &m))
        {
            for (int i = 0 ; i < n ; ++i)
                scanf("%d%d",&a[i].bg ,&a[i].ed);
            sort(a,a+n,cmp);
            for (int i = 0; i < 8; i++) 
                cout << a[i].ed;
            cout << "\n";
            for (int i = 0; i < 8; i++) 
                cout << a[i].bg;
                //cout << a[i].ed;

            cout << "\n";

            multiset<int>endtime; //h stores the end time of the ongoing activity in each classroom
            endtime.clear();
            int ans = 0;
            for (int i = 0 ; i < n ; ++i){
                cout << "a\n";
                multiset<int> :: iterator iter;
                iter = endtime.lower_bound(-a[i].bg); //Whether there is a classroom activity that ends before the start time of i
                for (std::multiset<int>::const_iterator i(endtime.begin()), end(endtime.end());
                    i != end;
                    ++i)
                    std::cout << *i << " primo\n";

                if (iter == endtime.end()){ //If there is no classroom that ends the activity before the event i starts, find another classroom
                    cout << "here\n";
                    if (endtime.size() < m){
                        endtime.insert(-a[i].ed- 1);
                        for (std::multiset<int>::const_iterator i(endtime.begin()), end(endtime.end());
                            i != end;
                            ++i)
                            std::cout << *i << " secondo\n";

                        ++ans;
                    }
                    continue;
                }
                endtime.erase(iter); //Found that a classroom activity is over, activity i is in this classroom
                endtime.insert( - a[i].ed - 1); //Update the end time of the event
                ++ans;
                for (std::multiset<int>::const_iterator i(endtime.begin()), end(endtime.end());
                            i != end;
                            ++i)
                            std::cout << *i << " terzo\n";
            }
        printf("%d\n" , ans);
        }
} 
