//Questa classe è fatta per calcolare il tempo trascorso tra 2 orari

//Questo lavoro è coperto dalla licenza descritta nel file LICENSE nella cartella UniPD.

//Leonardo Canello, Matricola 2032578, leonardocanello@protonmail.com, Telegram @Anulo
//2021/10/21, Università degli studi di Padova, Aula Taliercio

/*
Argomento: divisione intera </> e modulo <%>

Scrivere un programma che

    definisce due orari nel formato "24 ore", ciascuno di quattro cifre (ad esempio, 0900 oppure 1730)
    visualizza sull'output standard il numero di ore e di minuti (separatamente) che intercorrono fra i due orari

secondo il seguente esempio:
   primo orario: 0900
   secondo orario: 1730
  Tempo trascorso: 8 ore e 30 minuti

Modificare poi il programma in modo che funzioni correttamente anche se il secondo orario è inferiore al primo (per intervalli di tempo che comprendono la mezzanotte):
   primo orario: 1730
   secondo orario: 0900
  Tempo trascorso: 15 ore e 30 minuti
*/

import java.util.Scanner;

public class Esercizio7{
	public static void main (String[] pippo){
		final int  MINUTES_PER_HOUR = 60;
		final int MINUTES_PER_DAY = 24 * MINUTES_PER_HOUR;
		Scanner input = new Scanner(System.in);
		System.out.println("Inserisci il primo orario:");
		int ora1 = input.nextInt();
		System.out.println("Inserisci il secondo orario:");
		int ora2 = input.nextInt();

		if (ora1 >= 0 && ora1 < 2400 && ora1 >= 00 && ora2 < 2400){
			// Parte 1
			/*
			double dOra1 = (double)ora1/100;
			double dOra2 = (double)ora2/100;
			int iOra1 = (int)dOra1;
			int iOra2 = (int)dOra2;
			int iOre = Math.abs(iOra1-iOra2);
			dOra1 = dOra1 - iOra1;
			dOra2 = dOra2 - iOra2;
			double dOre = Math.abs(dOra1 - dOra2);
			System.out.println(iOre + " ore e " + (int)(dOre*100) + " minuti");
			*/

			// Parte 2
			int hOra1 = ora1 / 100;
			int hOra2 = ora2 / 100;
			int mOra1 = ora1 % 100 + hOra1 * MINUTES_PER_HOUR;
			int mOra2 = ora2 % 100 + hOra2 * MINUTES_PER_HOUR;;
			int timediff = mOra2 - mOra1;
			timediff = (timediff + MINUTES_PER_DAY);
			int hourdiff = timediff / MINUTES_PER_HOUR;
			int mindiff = timediff % MINUTES_PER_HOUR;
			int ore = hourdiff % 24;
			System.out.println(ore + " ore e " + mindiff + " minuti");

		}
		else {
			System.out.println("Uno dei valori è non valido!");
		}



	}
}
