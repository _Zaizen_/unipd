//Questa classe è fatta per mostrare il risultato di una divisione tra integer.

//Questo lavoro è coperto dalla licenza descritta nel file LICENSE nella cartella UniPD.

//Leonardo Canello, Matricola 2032578, leonardocanello@protonmail.com, Telegram @Anulo
//2021/10/21, Università degli studi di Padova, Aula Taliercio

/*
Argomento: rappresentazione dei numeri

Scrivere il codice di una classe IntegerDivider che definisce due numeri interi m e n e
invia a standard output il risultato e il resto della divisione m/n.
Compilare e eseguire.
Che cosa succede se il divisore è zero?
*/

public class IntegerDivider{
	public static void main(String[] pippo){
		int m,n;
		m = 5;
		n = 0;
		System.out.println(m + " / " + n + " = " + m / n + ", resto = " + m%n);
	}
}
