//Questa classe è fatta per provare la classe Rectangle

//Questo lavoro è coperto dalla licenza descritta nel file LICENSE nella cartella UniPD.

//Leonardo Canello, Matricola 2032578, leonardocanello@protonmail.com, Telegram @Anulo
//2021/10/21, Università degli studi di Padova, Aula Taliercio

/*
Argomento: uso della documentazione java

Esplorare la Documentazione API di Java, rintracciabile sia sul web che sul sito dell'Aula Taliercio (-> Guide on-line -> Java openjdk 11.0.12 API).
Cercare la documentazione della classe Rectangle (possibilmente con la funzione di ricerca del vostro Browser), studiarne la descrizione, studiare la descrizione dettagliata dei suoi metodi. In particolare:

    Identificare il metodo void add(int newx, int newy)
    Leggere con attenzione la documentazione
    Stabilire il risultato dell'esecuzione degli enunciati seguenti

  Rectangle box = new Rectangle(5, 10, 20, 30);
  box.add(0, 0);
*/

import java.awt.Rectangle;

public class Esercizio8{
	public static void main(String[] pippo){
		Rectangle  box = new Rectangle(5, 10, 20, 30);
		System.out.println(box);
		box.add(0, 0);
		System.out.println(box);
	}
}
