//Questa classe è fatta per stampare un numero di 5 ciifre separate da uno spazio ed aggiungere zeri in caso sia necessario.

//Questo lavoro è coperto dalla licenza descritta nel file LICENSE nella cartella UniPD.

//Leonardo Canello, Matricola 2032578, leonardocanello@protonmail.com, Telegram @Anulo
//2021/10/21, Università degli studi di Padova, Aula Taliercio

/*
Argomento: rappresentazione dei numeri; divisione intera </> e modulo <%>

Scrivere un programma che

    definisce un numero intero positivo avente al massimo cinque cifre
    visualizza sull'output standard le singole cifre del numero, separandole con uno spazio

Ad esempio, il numero 14356 verrà visualizzato come 1 4 3 5 6. Il numero 367 verrà visualizzato come 0 0 3 6 7. 

Suggerimento: applicare la definizione di notazione posizionale in base 10, studiare le proprietà della divisione intera in Java e del relativo operatore %. Verificare il corretto funzionamento del programma in casi diversi (in particolare, con numeri aventi meno di cinque cifre) e verificare il comportamento del programma in caso di valori di ingresso non ammessi

    numeri con più di cinque cifre
    numeri negativi
    numeri in virgola mobile
    valore di ingresso non numerico (ad esempio: Ciao)
*/

import java.util.Scanner;

public class Esercizio5{
	public static void main (String[] pippo){
		int numero;
		Scanner input = new Scanner(System.in);
		System.out.println("Inserisci il tuo numero:");
		numero = input.nextInt();
		if (numero >= 0){
			/*
			String buffer = Integer.toString(numero);
			while (buffer.length() < 5) buffer = "0"+buffer;
			System.out.println(buffer);
			String output = "";
			for (int i =0; i < buffer.length(); i++){
				output += buffer.charAt(i) + " ";
			}
			System.out.println(output);
			*/
			int n0 = numero%10;
            numero /= 10;
            int n1 = numero%10;
            numero /= 10;
            int n2 = numero%10;
            numero /= 10;
            int n3 = numero%10;
            numero /= 10;
            int n4 = numero%10;
            numero /= 10;
            System.out.println(n4 + " " + n3 + " " + n2 + " " + n1 + " " + n0);

		}
		else {
			System.out.println("Il tuo numero è negativo!");
		}
	}
}
