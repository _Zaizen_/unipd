//Questa classe è fatta per provare la classe Rectangle e l'intersezione

//Questo lavoro è coperto dalla licenza descritta nel file LICENSE nella cartella UniPD.

//Leonardo Canello, Matricola 2032578, leonardocanello@protonmail.com, Telegram @Anulo
//2021/10/21, Università degli studi di Padova, Aula Taliercio

/*
Argomento: uso di classi (in particolare uso di classi della libreria standard)

Il metodo intersection della classe Rectangle calcola l'intersezione di due rettangoli e viene invocato in questo modo

  Rectangle r3 = r1.intersection(r2);

(si veda anche la Documentazione API).
Scrivere un programma che

    costruisca due oggetti rettangolo, definendo per ciascuno di essi le coordinate del vertice in alto a sinistra, altezza e larghezza
    stampi vertici e dimensioni dei due rettangoli appena costruiti
    stampi vertici e dimensioni della loro intersezione.

Cosa succede quando i rettangoli non si sovrappongono?
*/

import java.awt.Rectangle;

public class Esercizio9{
	public static void main(String[] pippo){
		Rectangle r1 = new Rectangle(0, 0, 10, 10);
		Rectangle r2 = new Rectangle(5, 5, 10, 10);
		Rectangle r3 = r1.intersection(r2);
		System.out.println("\n\n I due rettangoli appena costruiti sono:");
		System.out.println(r1);
		System.out.println(r2);
		System.out.println("\n\n Il loro rettangolo intersezione e`:");
		System.out.println(r3);

		/*
			Aggiunta all'esercizio: cosa e` il metodo isEmpty()?
		*/
		if (box3.isEmpty())
		    System.out.println("Attenzione! Intersezione vuota!");
	    }
	}
}
