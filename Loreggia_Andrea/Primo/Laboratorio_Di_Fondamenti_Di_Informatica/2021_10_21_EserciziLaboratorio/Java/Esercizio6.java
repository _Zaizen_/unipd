//Questa classe è fatta per convertire un numero 

//Questo lavoro è coperto dalla licenza descritta nel file LICENSE nella cartella UniPD.

//Leonardo Canello, Matricola 2032578, leonardocanello@protonmail.com, Telegram @Anulo
//2021/10/21, Università degli studi di Padova, Aula Taliercio

/*
Argomento: rappresentazione dei numeri; divisione intera </> e modulo <%>

Scrivere un programma che definisca un numero razionale positivo n nell'intervallo [0, 32) e ne stampi a standard output la codifica binaria in formato a virgola fissa, usando cinque cifre binarie per la parte intera e cinque per quella frazionaria. Esempio: 8.5 in base dieci = 01000.10000 in base due.
Suggerimento: si usino gli algoritmi di conversione da decimale a binario della parte intera e della parte frazionaria presentati a lezione.
*/

import java.util.Scanner;

public class Esercizio6{
	public static void main (String[] pippo){
		final int  BASE = 2;
		Scanner input = new Scanner(System.in);
		System.out.println("Inserisci un numero:");
		double numero = input.nextDouble();
		if (numero >= 0 && numero < 32){
			int intera = (int) numero;
			String sIntera = "";
			
			int cifra = intera  % BASE;
			sIntera = cifra + sIntera;
			
			intera = intera / BASE;
			cifra = intera % BASE;
			sIntera = cifra + sIntera;
			
			intera =  intera / BASE;
			cifra = intera % BASE;
			sIntera = cifra + sIntera;
			
			intera =  intera / BASE;
			cifra = intera % BASE;
			sIntera = cifra + sIntera;
			
			intera =  intera / BASE;
			cifra = intera % BASE;
			sIntera = cifra + sIntera;
			
			System.out.println(sIntera);
			
			
			double frazione = numero - (int) numero;
			String sFrazione = "";
			
			System.out.println(frazione);
			cifra = (int) (BASE * frazione);
			sFrazione = sFrazione + cifra;
			frazione = (BASE * frazione) - cifra;
			
			cifra = (int) (BASE * frazione);
			sFrazione = sFrazione + cifra;
			frazione = (BASE * frazione) - cifra;
			
			cifra = (int) (BASE * frazione);
			sFrazione = sFrazione + cifra;
			frazione = (BASE * frazione) - cifra;
			
			cifra = (int) (BASE * frazione);
			sFrazione = sFrazione + cifra;
			frazione = (BASE * frazione) - cifra;
			
			cifra = (int) (BASE * frazione);
			sFrazione = sFrazione + cifra;
			
			System.out.println(sFrazione);
			
			System.out.println(sIntera + "." + sFrazione);
			
		}
		else {
			System.out.println("Valore non valido!");
		}
		
		
	
	}
}
