//Questa classe è fatta per mostrare il risultato di una divisione tra double.

//Questo lavoro è coperto dalla licenza descritta nel file LICENSE nella cartella UniPD.

//Leonardo Canello, Matricola 2032578, leonardocanello@protonmail.com, Telegram @Anulo
//2021/10/21, Università degli studi di Padova, Aula Taliercio

/*
Argomento: rappresentazione dei numeri

Scrivere poi il codice di una classe DoubleDivider che definisce due numeri reali x e y e
invia a standard output il risultato della divisione x/y.
Compilare ed eseguire. Esegue correttamente?
Provare con le coppie di numeri:
*/

public class DoubleDivider{
	public static void main (String[] args){
		double x,y;
		x = 7.0;
		y = 0.0;
		System.out.println(x + " / " + y + " = " + x / y);
		x = -7.0;
		y = 0.0;
		System.out.println(x + " / " + y + " = " + x / y);
		x = 0.0;
		y = 0.0;
		System.out.println(x + " / " + y + " = " + x / y);
	}
}
