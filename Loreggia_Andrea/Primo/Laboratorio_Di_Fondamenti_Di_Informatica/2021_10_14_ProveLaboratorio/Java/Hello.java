//Questa classe è un classico Hello World proposto dal tutor. 

//Questo lavoro è coperto dalla licenza descritta nel file LICENSE nella cartella UniPD.

//Leonardo Canello, Matricola 2032578, leonardocanello@protonmail.com, Telegram @Anulo
//2021/10/14, Università degli studi di Padova, Aula Taliercio

public class Hello{
	public static void main (String[] pippo){
		System.out.println("Hello, World!");
	}
}
