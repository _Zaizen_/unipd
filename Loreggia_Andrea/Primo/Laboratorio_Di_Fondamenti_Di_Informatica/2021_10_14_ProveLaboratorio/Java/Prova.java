// Questa classe è una prova degli strumenti disponibili in laboratorio Taliercio dell' UniPD

//Questo lavoro è coperto dalla licenza descritta nel file LICENSE nella cartella UniPD. 

//Leonardo Canello, Matricola 2032578, leonardocanello@protonmail.com, Telegram @Anulo
//2021/10/14, Università degli studi di Padova, Aula Taliercio

public class Prova{
	public static void main (String[] pippo){
		System.out.println("Hello, World!");
		Scanner input = new Scanner(System.in);
		String ciao = input.nextLine();
		System.out.println(ciao);
	}
}
