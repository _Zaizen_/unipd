In questa cartella risiede tutto il lavoro prodotto in aula Taliercio dell' Università degli studi di Padova da parte di Leonardo Canello.
Sono inoltre presenti appunti ed altri materiali in LaTeX come ad esempio gli appunti di Analisi 1.
In caso si voglia contribruire al mio  operato si è benvenuti.



Shield: [![CC BY-NC-SA 4.0][cc-by-nc-sa-shield]][cc-by-nc-sa]

This work is licensed under a
[Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License][cc-by-nc-sa].

[![CC BY-NC-SA 4.0][cc-by-nc-sa-image]][cc-by-nc-sa]

[cc-by-nc-sa]: http://creativecommons.org/licenses/by-nc-sa/4.0/
[cc-by-nc-sa-image]: https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png
[cc-by-nc-sa-shield]: https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg
